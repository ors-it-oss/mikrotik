/partitions 
:if ([:len $1] = 0) do={
    :put "usage: [save|restore]";
    :return 1;
}

:if ([get [find active=yes] name] != "downgrade") do={ 
    set [find active=yes && name!=downgrade && name!=running] name=running fallback-to=backup
    set [find active=no && name!=downgrade && name!=backup] name=backup fallback-to=downgrade

    :if ($1 = "save") do={
        #:put ("RouterOS v" . [/system package get system version] . " .*");
        #:put [get [find name=backup] version];

        :if ([get [find name=backup] version] ~ ("RouterOS v" . [/system package get system version] . " .*")) do={
            save-config-to backup;
        } else={
            copy-to backup;
        }
    }

    :if ($1 = "restore") do={
        restore-config-from backup;
    }
} else={
   :put "Running on the downgrade partition, not doing a thing!"
}
