add chain=forward disabled=yes action=log log-prefix="MULTI\?" packet-type=multicast

add action=drop chain=forward comment="PVLAN mode\?" in-bridge=br-guests

add chain=input comment="Accept IPv6 traffic" in-bridge=br-www mac-protocol=ipv6
add chain=input comment="Accept IP traffic to me" dst-address=82.176.206.154/32 mac-protocol=ip
add chain=input comment="Accept good ARP" dst-mac-address=DE:0F:11:05:0F:1E/FF:FF:FF:FF:FF:FF in-bridge=br-www mac-protocol=arp
add action=drop arp-dst-address=!82.176.206.154/32 arp-opcode=request chain=input comment="Stupid ZeelandNet ARP requests" in-bridge=br-www mac-protocol=arp
add chain=input comment="Accept ARP\?" disabled=yes in-bridge=br-www mac-protocol=arp packet-type=broadcast
add chain=input comment="Accept DHCP" dst-port=68 in-bridge=br-www ip-protocol=udp mac-protocol=ip packet-type=broadcast src-port=67
add action=log chain=input comment="Log Traffic that's 'odd'" in-bridge=br-www log-prefix=BR-STR
add chain=output comment="Accept from my IP" mac-protocol=ip out-bridge=br-www src-address=82.176.206.154/32
add chain=output comment="Accept Host IPv6 (WTF MikroTik!\?)" mac-protocol=ipv6 out-bridge=br-www packet-type=host
add chain=output comment="ARP is acceptable" mac-protocol=arp out-bridge=br-www
add chain=output comment="STP, LLDP, Ethernet stuff" dst-mac-address=01:80:C2:00:00:00/FF:FF:FF:FF:FF:00 mac-protocol=0x27
add chain=output mac-protocol=0x27
add chain=output comment="Accept DHCP" dst-port=67 ip-protocol=udp mac-protocol=ip out-bridge=br-www packet-type=host src-port=68
add action=log chain=output log-prefix=OUTARP-TF out-bridge=br-www
