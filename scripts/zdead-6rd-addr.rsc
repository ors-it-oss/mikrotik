:local WANif "br-www"
:local ipv6prefix "2a02:f6a:"
:local ipv6suffix "::"

:global IPv4addr 
:foreach if in=[/ip address find interface=$WANif dynamic=yes] do={ 
	:global IPv4addr [:tostr [/ip address get $if address]]
	:global IPv4addr [:pick $IPv4addr 0 [:find $IPv4addr "/"]]
}
:put "IPv4 Address $IPv4addr";
:local IPv6temp [:toip6 ("1::" . $IPv4addr)]

:if ($IPv6temp="") do={
    :error "IPv6 temporary address error"
}

:put "IPv6 Temp $IPv6temp"
:local IPv4hex [:pick $IPv6temp 3 15]
:put "IPv4 Hex $IPv4hex"
:local IPv6addr [($ipv6prefix . $IPv4hex . $ipv6suffix)]
:put "IPv6 6rd Address $IPv6addr"

:local IPv6addrnumber [/ipv6 address find where comment=$addrcomment]
:if ($IPv6addrnumber="") do={
    :error "IPv6 Address Number error"
}

:put "IPv6 address number $IPv6addrnumber"
