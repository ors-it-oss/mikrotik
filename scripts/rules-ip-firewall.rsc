add chain=forward connection-state=invalid action=log log-prefix=FW-INV:
add chain=forward connection-state=invalid action=drop 
add chain=forward connection-state=established action=accept
add chain=forward connection-state=related action=accept
add chain=forward jump-target=check-tcp protocol=tcp action=jump 
add chain=forward jump-target=check-udp protocol=udp action=jump 
add chain=forward disabled=yes in-interface=br666-www action=drop 
add chain=forward action=jump jump-target=fwd-users in-interface=br410-users
add     chain=fwd-users out-interface=br666-www action=accept
add     chain=fwd-users out-interface=br010-netdevs action=accept
add action=jump chain=forward in-interface=br060-media jump-target=fwd-media

add chain=input connection-state=invalid log-prefix=IN-INV action=log
add chain=input connection-state=invalid action=drop
add chain=input protocol=icmp jump-target=in-icmp action=jump 
add     chain=in-icmp dst-limit=8,32,dst-address/15m
add     chain=in-icmp action=reject dst-limit=60,512,dst-address/15m reject-with=icmp-host-prohibited
add     chain=in-icmp action=add-src-to-address-list address-list=global-block address-list-timeout=1w 
add     chain=in-icmp action=drop
add chain=input connection-state=established action=accept
add chain=input connection-state=related action=accept
add chain=input in-interface=br010-netdevs
add action=drop chain=input connection-state=new in-interface=br666-www
add chain=input dst-address=10.7.16.0/24 dst-port=53 protocol=udp
add chain=input dst-address=10.7.16.0/24 dst-port=53 protocol=tcp
add chain=input dst-address=10.7.1.1 dst-port=22 protocol=tcp
add chain=input dst-address=10.7.1.1 dst-port=80 protocol=tcp
add chain=input dst-address=10.7.1.1 dst-port=443 protocol=tcp
add action=log chain=input disabled=yes log-prefix=INPUT-HUH

add chain=output connection-state=established
add chain=output connection-state=related
add chain=output dst-port=53 out-interface=br666-www protocol=udp src-address-list=us
add chain=output dst-port=123 out-interface=br666-www protocol=udp src-address-list=us src-port=123
add chain=output disabled=yes dst-address-list=znet-6rdend out-interface=br666-www protocol=ipv6
add chain=output disabled=yes action=log connection-state=new log-prefix=OUT-NEW
add chain=output disabled=yes action=log 

add chain=check-udp comment="UDP port 0 attacks" src-port=0 action=drop protocol=udp 
add chain=check-udp comment="UDP port 0 attacks" dst-port=0 action=drop protocol=udp

add chain=check-tcp comment="TCP port 0 attacks" dst-port=0 action=drop protocol=tcp 
add chain=check-tcp comment="TCP port 0 attacks" src-port=0 action=drop protocol=tcp 

add chain=check-tcp comment="malformed TCP flags" tcp-flags=fin,!ack action=drop protocol=tcp 
add chain=check-tcp comment="malformed TCP flags" tcp-flags=rst,urg action=drop protocol=tcp 
add chain=check-tcp comment="malformed TCP flags" tcp-flags=fin,urg action=drop protocol=tcp 
add chain=check-tcp comment="malformed TCP flags" tcp-flags=fin,syn action=drop protocol=tcp 
add chain=check-tcp comment="malformed TCP flags" tcp-flags=fin,rst action=drop protocol=tcp 
add chain=check-tcp comment="malformed TCP flags" tcp-flags=syn,rst action=drop protocol=tcp 
add chain=check-tcp comment="malformed TCP flags" tcp-flags=!fin,!syn,!rst,!ack action=drop protocol=tcp 
add chain=check-tcp comment="TCP Null scan" tcp-flags=fin,psh,urg,!syn,!rst,!ack action=drop protocol=tcp 
add chain=check-tcp comment="TCP Xmas scan" tcp-flags=!fin,!syn,!rst,!psh,!ack,!urg action=drop protocol=tcp 
#add chain=check-tcp comment="NMAP FIN Stealth scan" tcp-flags=fin,!syn,!rst,!psh,!ack,!urg action=drop protocol=tcp 
#add chain=check-tcp comment="NMAP NULL scan" tcp-flags=!fin,!syn,!rst,!psh,!ack,!urg action=drop protocol=tcp 

#add chain=input protocol=tcp tcp-flags= action=add-src-to-address-list address-list="port scanners" address-list-timeout=2w comment=""
#add chain=input protocol=tcp tcp-flags=fin,syn action=add-src-to-address-list address-list="port scanners" address-list-timeout=2w comment="SYN/FIN scan"
#add chain=input protocol=tcp tcp-flags=syn,rst action=add-src-to-address-list address-list="port scanners" address-list-timeout=2w comment="SYN/RST scan"
#add chain=input protocol=tcp tcp-flags=fin,psh,urg,!syn,!rst,!ack action=add-src-to-address-list address-list="port scanners" address-list-timeout=2w comment="FIN/PSH/URG scan"
#add chain=input protocol=tcp tcp-flags=fin,syn,rst,psh,ack,urg action=add-src-to-address-list address-list="port scanners" address-list-timeout=2w comment="ALL/ALL scan"
