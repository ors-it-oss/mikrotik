### Time @offset function
### This script will calculate some time/date 
###
### SYNTAX:
### $1 = "+/-MikroTik time" - Offset into the future / past
### date="MikroTik date"
### time="MikroTik time" 
###       Date/Time to start from rather than now/now
### dst=false/true - Correct for Daylight Savings Time when necessary 
###
### EXAMPLES:
### $time "+10m" 
### $time "+11:11:11" time="10:10:10"
### $time "-10h" date="feb/12/2012"
### $time "-1w9d30h" date="jun/30/2014" time="12:54:12" 
###
### RETURNS: { str newdate ; time newtime }
###
### REQUIRES:
### * For DST calculation; a global $wday function which returns day of the week for a given date as { 0 ; sun }
###
###

#################
### CONSTANTS ###
#################
:local months { "jan";"feb";"mar";"apr";"may";"jun";"jul";"aug";"sep";"oct";"nov";"dec" };

#################
### INPUT #######
#################
:local op [:pick $1 0 1];
:local offset [:totime [:pick $1 1 [:len $1]]];
:local indate $date;
:local intime $time;
:local indst;
:if ([:len $dst] > 0) do={
    :set indst $dst;
} else={
    :set indst true;
}
#Figure out the current/from date/time
:local date;
:local time;
:if ([:len $indate] > 0) do={
    :set date $indate;
} else={
    :set date [/system clock get date];
}
:if ([:len $intime] > 0) do={
    :set time $intime;
} else={
    :set time [/system clock get time];
}

#################
### FUNCTIONS ###
#################
:local inDST do={ 
    #Check whether a date is within DST
    #Syntax: $date-array $goto-the-future
    # The current European guidelines are:
    # Starts on the last Sunday of March @ 01:00 UTC (02:00 CET)
    # Stops on the last Sunday of October @ 01:00 UTC (03:00 CET)

    #Function that returns the day of the week as { 0 ; sun }
    :global wday;
    :local inDay (300 + (31 - [:pick [$wday ("mar/31/" . ($1->"year"))] 0]));
    :local outDay (1000 + (31 - [:pick [$wday ("oct/31/" . ($1->"year"))] 0]));

    :local inTime 20000;
    :local outTime 30000;    
    
#    :put "In $($1->"year"), DST starts at March $inDay $inTime, and stops at Oct $outDay $outTime";
    
    :local date (($1->"month") * 100 + ($1->"day"));
    #:put "Comparing $inDay and $outDay with $date";

    :if ($date = $inDay) do={
        :set date (($1->"hour") * 10000 + ($1->"minute") * 100 + ($1->"second"));
        :if (($2 && $date >= $inTime) || (!$2 && $date >= $outTime)) do={
#            :put "It's IN!";
            :return true;
        } else={
            :return false;
        }
    }
    :if ($date = $outDay) do={
        :set date (($1->"hour") * 10000 + ($1->"minute") * 100 + ($1->"second"));
#        :put "Direction: $2, Date: $date, Intime: $inTime, outTime: $outTime";
        :if (($2 && $date <= $outTime) || (!$2 && $date <= $inTime)) do={
#            :put "It's IN!";
            :return true;
        } else={
            :return false;
        }
    }

    :if ($date > $inDay && $date < $outDay) do={
        :return true;
    } else={
        :return false;
    }
}

:local offsetTime do={
    #$year, $month, $day, $hour, $minute, $second
    #$op, $future
    #$days, $hours, $minutes, $seconds
    :local year ($date->"year");
    :local month ($date->"month"); 
    :local day ($date->"day");
    :local hour ($date->"hour"); 
    :local minute ($date->"minute"); 
    :local second ($date->"second");
    :local op ($offset->"op");
    :local future ($offset->"future");
    :local days ($offset->"days");
    :local hours ($offset->"hours");
    :local minutes ($offset->"minutes");
    :local seconds ($offset->"seconds");
    
    :if ($seconds > 0) do={
        :set second ($second + [:tonum ($op . $seconds)]);
        :if ($second >= 60) do={
            :set minutes ($minutes + 1);
            :set second ($second - 60);
        } else={
            :if ($second < 0) do={
                :set minutes ($minutes + 1);
                :set second (60 + $second);
    } ; } ; } 

    :if ($minutes > 0) do={
        :set minute ($minute + [:tonum ($op . $minutes)]);
        :if ($minute >= 60) do={
            :set hours ($hours + 1);
            :set minute ($minute - 60);
        } else={
            :if ($minute < 0) do={
                :set hours ($hours + 1);
                :set minute (60 + $minute);
    } ; } ; }

    :if ($hours > 0) do={
        :set hour ($hour + [:tonum ($op . $hours)]);
        :if ($hour >= 24) do={
            :set days ($days + 1);
            :set hour ($hour - 24);
        } else={
            :if ($hour < 0) do={
                :set days ($days + 1);
                :set hour (24 + $hour);
    } ; } ; }

#    :put "$day/$month/$year $time (in DST: $dst), offset: $days days @ $hour:$minute:$second";

    if ([:len $days] > 0 && $days > 0) do={
        #But first, another function. 
        #Gotta love the expansive selection of scopes...
        :local getMonths do={
            #Get all month dates for year $1 
            #& deal with leap years!

            #But first, another function. 
            #Gotta love the expansive selection of scopes...
            #:local mod do={ :return ($1 - (($1 / $2) * $2)) };
            :local leap false;
            :if (($1 % 4) = 0) do={
                :if (($1 % 100) = 0) do={
                    :if (($1 % 400) = 0) do={ :set leap true }
                } else= { :set leap true }
            }

            :local days;    
            :if ($leap) do={
                :set leap 29; :set days 366;
            } else={
                :set leap 28; :set days 365;		
            }

            :return { $days;31;$leap;31;30;31;30;31;31;30;31;30;31 };
        }

        :local length [$getMonths $year];
        :local left;
        :if ($future) do={
            :set left ([:pick $length $month] - $day);        
        } else={
            :set left $day;
        }

        #:put "Left: $left, days: $days";
    
        :if (($days <= $left && $future) || ($days < $left && !$future)) do={
            #:put "Not looping";
            :set day ($day + [:tonum ($op . $days)]);
        } else={
            #:put "days: $days, left: $left, day: $day";
       
            :set days ($days - $left);
            :set month ($month + [:tonum ($op . 1)]);
            :while ($days > [:pick $length 0]) do={
                :set days ($days - [:pick $length 0]);
                :set year ($year + [:tonum ($op . 1)]);
                :set length [$getMonths $year];
                #:put "After $year, $days days left";
            }
            #:put "$days days left";
            :while ($days >= [:pick $length $month]) do={
                :set days ($days - [:pick $length $month]);
                #:put "After $month in $year, $days left";
                :set month ($month + [:tonum ($op . 1)]);
                :if ($month = 0) do={
                    :set month 12;
                    :set year ($year + [:tonum ($op . 1)]);
                    :set length [$getMonths $year];
                }
                :if ($month = 13) do={
                    :set month 1;
                    :set year ($year + [:tonum ($op . 1)]);
                    :set length [$getMonths $year];
                }
                #:put "Month is now $month, and it has $[:pick $length $month] days";
            }
            #:put "$days days left"; 
            :if ($future) do={
                :set day $days;
            } else={
                :set day ([:pick $length $month] - $days);
    } ; } ; }

    :return { 
        year=$year; month=$month; day=$day;
        hour=$hour; minute=$minute; second=$second;
    }
}

#################
### EXECUTION ###
#################
#Cleanup the offset
#Sometimes this stupid dot will show up
:local pos [:find $offset "."];
if ([:len $pos] > 0) do={
    :set offset [:pick $offset 0 $pos];
}
#:put "offset after dot: $offset";

:local days;
:set pos [:find $offset "w"];
if ([:len $pos] > 0) do={
    :set days (7 * [:pick $offset 0 $pos]);
    :set offset [:pick $offset ($pos + 1) [:len $offset]];
}
#:put "offset after weeks: $offset"

:set pos [:find $offset "d"];
if ([:len $pos] > 0) do={
    :set days ($days + [:pick $offset 0 $pos]);
    :set offset [:pick $offset ($pos + 1) [:len $offset]];
}
#:put "offset after days: $offset"

:set offset {
    days=$days;
    hours=[:tonum [:pick $offset 0 2]];
    minutes=[:tonum [:pick $offset 3 5]];
    seconds=[:tonum [:pick $offset 6 8]];
}

:if ($op = "-") do={
    :set ($offset->"future") false;
    :set ($offset->"op") $op;
} else={
    :set ($offset->"future") true;
}

:set date {
    year=[:tonum [:pick $date 7 11]];
    month=([:find $months [:pick $date 0 3]] + 1);
    day=[:tonum [:pick $date 4 6]];
    hour=[:tonum [:pick $time 0 2]];
    minute=[:tonum [:pick $time 3 5]];
    second=[:tonum [:pick $time 6 8]];
}
#:put "offsetting $date by $offset";

if ($indst) do={
    if ([:len $indate] = 0 && [:len $intime] = 0) do={
        :set ($date->"dst") [/system clock get dst-active];
    } else={
        :set ($date->"dst") [$inDST $date ($offset->"future")];
    }
}

#:put ("offsetting " . [:pick $months (($date->"month") - 1)] . "/" . ($date->"day") . "/" . ($date->"year") . " by " );

:local tdate [$offsetTime date=$date offset=$offset];

if ($indst && ($date->"dst") != [$inDST $tdate ($offset->"future")]) do={
#    :put "Begin: $date, Target: $month/$day/$year $hour:$minute:$second";

    if (($date->"dst")) do={
        :set offset {
            hours=1; op="-" ; future=false;
        }
    } else={
        :set offset {
            hours=1; future=true;
        }
    }
    :set tdate [$offsetTime date=$tdate offset=$offset];

#    :put "Post-DST target: $date";
}

:set date ([:pick $months (($tdate->"month") - 1)] . "/" . ($tdate->"day") . "/" . ($tdate->"year"));
:set time [:totime (($tdate->"hour") . ":" . ($tdate->"minute") . ":" . ($tdate->"second"))];

:return { $date; $time };
