:local WANif "br-www"
:local fwGW "www-GW"
:local fwAddr "www-Addr"

/ip firewall address-list set [find list=$fwGW] address=[/ip dhcp-client get [find interface=$WANif] gateway]

:local IPaddr [:tostr [/ip dhcp-client get [find interface=$WANif] address]]
:local IPaddr [:pick $IPaddr 0 [:find $IPaddr "/"]]
/ip firewall address-list set [find list=$fwAddr] address=$IPaddr