:global gfhash;
:global fnv;

:if ([:len $gfhash] = 0) do={
    :set gfhash { fnv = 0 };
}

:local script "gf-explode"; 
:local name "turd";
:local source [/system script get $script source]; 
:local hash [$fnv $script];

:local exec;
#:if ($hash = ($gfhash->$script)) do={
#    :set exec [:parse ":global $name"];
#} else={
    :set exec [:parse ":global $name [:parse [/system script get $script source]]"];
#    :set ($gfhash->$script) $hash;    
#}
$exec;

:error "BYE"


#Generic tools
#:global md5sum [:parse [/system script get gf-hash-md5sum-broken source]];



:global fnv [:parse [/system script get gf-hash-fnv-1a source]];
:global gf [:parse [/system script get global-functions source]];
:global toarray [:parse [/system script get gf-explode source]];
:global part [:parse [/system script get gf-partitions source]];

#Date/time stuff
:global time [:parse [/system script get gf-time source]];
:global wday [:parse [/system script get gf-weekday source]]; 

#process rulesets
:global pr [:parse [/system script get gf-rulesets source]];
:global commit do={ $pr save };
:global revert do={ $pr revert};