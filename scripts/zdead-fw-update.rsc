/ip firewall filter
:put "IPv4: Marking old rules..."
set [find] comment=TOBEDEVOURED
:put "IPv4: Adding new rulez..." 
:local rules [:parse "/ip firewall filter;$[ /system script get fw-rules source ]"];
$rules;
:put "IPv4: Adding temporary DROP rules..."
add chain=input action=drop place-before=0 comment="UPDATING"
add chain=forward action=drop place-before=0 comment="UPDATING"
add chain=output action=drop place-before=0 comment="UPDATING"
:put "IPv4: Removing old rules..."
remove [find comment=TOBEDEVOURED]
:put "IPv4: Removing temporary DROP rules..."
remove [find comment=UPDATING]
