#:local name floep;
#:local fiet { kee="bral";blo="pass";alp="bernard" };
#:local bal { "kee";"pass";1;"15";"bernard" };
#:local fla { 2=0;5=2;3=4;5=10 };

:local input $1;
:local sorter { 0 };

:foreach v in=$input do={
   :set ($sorter->[:tostr $v]) $v; 
}

#:put "HI1";
#:put $sorter;

:set sorter [:pick $sorter 1 [:len $sorter]];

#:put "HI";
#:put $sorter;

:local sorted { 0 };

#:put "HI"
#:put $sorted;

:foreach k,v in=$sorter do={
#   :put "Inputting $k/$v";
   :set sorted ($sorted,$v); 
}

:set sorted [:pick $sorted 1 [:len $sorted]];

#:put "HI"
#:put $sorted;
:return $sorted;
